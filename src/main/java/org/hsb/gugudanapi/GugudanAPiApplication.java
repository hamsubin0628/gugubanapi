package org.hsb.gugudanapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GugudanAPiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GugudanAPiApplication.class, args);
    }

}
